package com.iwec.books.repository;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.iwec.books.model.Author;

@Component
public class AuthorRepository {
	private static final Map<Integer, Author> authors = new TreeMap<>();

	@PostConstruct
	public void initData() {
		Author author = new Author();
		author.setAuthorId(1);
		author.setFirstName("Zora");
		author.setLastName("Bendeva");
		authors.put(author.getAuthorId(), author);

		author = new Author();
		author.setAuthorId(2);
		author.setFirstName("Zoran");
		author.setLastName("Kotevski");
		authors.put(author.getAuthorId(), author);

		author = new Author();
		author.setAuthorId(3);
		author.setFirstName("Gordana");
		author.setLastName("Trajkovska");
		authors.put(author.getAuthorId(), author);
	}

	public Author findAuthorById(Integer id) {
		Assert.notNull(id, "The Author's id must not be null");
		return authors.get(id);
	}

	public List<Author> findAllAuthors() {
		List<Author> result = new LinkedList<>(authors.values());
		return result;
	}

	public Integer saveOrUpdate(Author author) {
		Integer id = author.getAuthorId();
		Integer key = (author == null || id == null) ? authors.size() + 1 : id;
		authors.put(key, author);
		return author.getAuthorId();
	}

	public boolean deleteAuthorById(Integer id) {
		Author author = authors.remove(id);
		return (author != null ? true : false);
	}
}
