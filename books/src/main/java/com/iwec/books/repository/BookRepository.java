package com.iwec.books.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.iwec.books.model.Book;

@Component
public class BookRepository {
	private static final Map<Integer, Book> books = new TreeMap<>();
	private static final Integer id1 = 1;
	private static final Integer id2 = 2;
	private static final Integer id3 = 3;

	@PostConstruct
	public void initData() {
		AuthorRepository author = new AuthorRepository();
		Book book = new Book();
		book.setBookId(1);
		book.setTitle("Soap Web Services");
		book.getAuthors().add(author.findAuthorById(id1));
		book.setPublisher("InterWorks");
		book.setYear(2017);
		book.setPrice(250);
		books.put(book.getBookId(), book);

		book = new Book();
		book.setBookId(2);
		book.setTitle("Rest Web Services");
		book.getAuthors().add(author.findAuthorById(id2));
		book.getAuthors().add(author.findAuthorById(id3));
		book.setPublisher("Integra Ultra");
		book.setYear(2018);
		book.setPrice(300);
		books.put(book.getBookId(), book);

		book = new Book();
		book.setBookId(3);
		book.setTitle("Data Formats");
		book.getAuthors().add(author.findAuthorById(id1));
		book.getAuthors().add(author.findAuthorById(id2));
		book.getAuthors().add(author.findAuthorById(id3));
		book.setPublisher("Libi");
		book.setYear(2019);
		book.setPrice(500);
		books.put(book.getBookId(), book);
	}

	public Book findBookById(Integer id) {
		Assert.notNull(id, "The Book id must not be null");
		return books.get(id);
	}

	public List<Book> findAllBooks() {
		List<Book> result = new ArrayList<>(books.values());
		return result;
	}

	public Integer saveOrUpdate(Book book) {
		Integer id = book.getBookId();
		Integer key = (book == null || id == null) ? books.size() + 1 : id;
		books.put(key, book);
		return id;
	}

	public boolean deleteBookById(Integer id) {
		Book book = books.remove(id);
		return (book != null ? true : false);
	}
}
