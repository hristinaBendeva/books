package com.iwec.books.endpoint;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.iwec.books.model.AddAuthorRequest;
import com.iwec.books.model.AddAuthorResponse;
import com.iwec.books.model.Author;
import com.iwec.books.model.AuthorDetailsRequest;
import com.iwec.books.model.AuthorDetailsResponse;
import com.iwec.books.model.AuthorListResponse;
import com.iwec.books.model.DeleteAuthorRequest;
import com.iwec.books.model.DeleteAuthorResponse;
import com.iwec.books.model.ServiceStatus;
import com.iwec.books.model.UpdateAuthorRequest;
import com.iwec.books.model.UpdateAuthorResponse;
import com.iwec.books.repository.AuthorRepository;

@Endpoint
public class AuthorEndpoint {
	private static final String NAMESPACE_URI = "model.books.iwec.com";

	@Autowired
	private AuthorRepository authorRepository;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "AuthorDetailsRequest")
	@ResponsePayload
	public AuthorDetailsResponse getAuthor(@RequestPayload AuthorDetailsRequest request) {
		Author author = authorRepository.findAuthorById(request.getAuthorId());
		AuthorDetailsResponse response = new AuthorDetailsResponse();
		response.setAuthor(author);
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "AuthorListRequest")
	@ResponsePayload
	public AuthorListResponse getAllAuthors() {
		AuthorListResponse response = new AuthorListResponse();
		List<Author> result = authorRepository.findAllAuthors();
		response.getAuthor().addAll(result);
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addAuthorRequest")
	@ResponsePayload
	public AddAuthorResponse addAuthor(@RequestPayload AddAuthorRequest request) {
		ServiceStatus status = new ServiceStatus();
		AddAuthorResponse response = new AddAuthorResponse();
		Author a = new Author();
		a.setAuthorId(request.getAuthorId());
		a.setFirstName(request.getFirstName());
		a.setLastName(request.getLastName());
		Integer author = authorRepository.saveOrUpdate(a);
		if (author == null || request.getAuthorId() == 0) {
			status.setStatusCode("Fail");
			status.setMessage("Insert Unsuccessful");
			response.setServiceStatus(status);
		} else {
			status.setStatusCode("Success");
			status.setMessage("An Author has been added");
			response.setAuthor(a);
			response.setServiceStatus(status);
		}
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateAuthorRequest")
	@ResponsePayload
	public UpdateAuthorResponse updateAuthor(@RequestPayload UpdateAuthorRequest request) {
		Author a = new Author();
		a.setAuthorId(request.getAuthor().getAuthorId());
		a.setFirstName(request.getAuthor().getFirstName());
		a.setLastName(request.getAuthor().getLastName());
		Integer author = authorRepository.saveOrUpdate(a);
		ServiceStatus status = new ServiceStatus();
		UpdateAuthorResponse response = new UpdateAuthorResponse();
		status.setStatusCode("Success");
		status.setMessage("An Author has been udated");
		response.setServiceStatus(status);
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteAuthorRequest")
	@ResponsePayload
	public DeleteAuthorResponse removeAuthor(@RequestPayload DeleteAuthorRequest request) {
		Author author = authorRepository.findAuthorById(request.getAuthorId());
		ServiceStatus status = new ServiceStatus();
		if (author == null) {
			status.setStatusCode("false");
			status.setMessage("delete unsuccessful");
		} else {
			authorRepository.deleteAuthorById(author.getAuthorId());
			status.setStatusCode("true");
			status.setMessage("Delete successful");
		}
		DeleteAuthorResponse response = new DeleteAuthorResponse();
		response.setServiceStatus(status);
		return response;
	}
}