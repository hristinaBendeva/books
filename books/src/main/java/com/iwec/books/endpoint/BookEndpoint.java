package com.iwec.books.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.iwec.books.model.AddBookRequest;
import com.iwec.books.model.AddBookResponse;
import com.iwec.books.model.Book;
import com.iwec.books.model.BookDetailsRequest;
import com.iwec.books.model.BookDetailsResponse;
import com.iwec.books.model.BookListResponse;
import com.iwec.books.model.DeleteBookRequest;
import com.iwec.books.model.DeleteBookResponse;
import com.iwec.books.model.ServiceStatus;
import com.iwec.books.repository.BookRepository;

@Endpoint
public class BookEndpoint {
	private static final String NAMESPACE_URI = "model.books.iwec.com";

	@Autowired
	private BookRepository bookRepository;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "BookDetailsRequest")
	@ResponsePayload
	public BookDetailsResponse getBook(@RequestPayload BookDetailsRequest request) {
		Book book = bookRepository.findBookById(request.getBookId());
		BookDetailsResponse response = new BookDetailsResponse();
		response.setBook(book);
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "BookListRequest")
	@ResponsePayload
	public BookListResponse getAllBooks() {
		BookListResponse response = new BookListResponse();
		List<Book> result = bookRepository.findAllBooks();
		response.getBook().addAll(result);
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addBookRequest")
	@ResponsePayload
	public AddBookResponse addBook(@RequestPayload AddBookRequest request) {
		ServiceStatus status = new ServiceStatus();
		AddBookResponse response = new AddBookResponse();
		Book b = new Book();
		b.setBookId(request.getBookId());
		b.setTitle(request.getTitle());
		b.getAuthors().addAll(request.getAuthors());
		b.setPublisher(request.getPublisher());
		b.setYear(request.getYear());
		b.setPrice(request.getPrice());
		Integer book = bookRepository.saveOrUpdate(b);
		if (book == null || request.getBookId() == 0) {
			status.setStatusCode("Fail");
			status.setMessage("Insert Unsuccessful");
			response.setServiceStatus(status);
		} else {
			status.setStatusCode("Success");
			status.setMessage("A Book has been added");
			response.setBook(b);
			response.setServiceStatus(status);
		}
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteBookRequest")
	@ResponsePayload
	public DeleteBookResponse removeBook(@RequestPayload DeleteBookRequest request) {
		Book book = bookRepository.findBookById(request.getBookId());
		ServiceStatus status = new ServiceStatus();
		if (book == null) {
			status.setStatusCode("false");
			status.setMessage("delete unsuccessful");
		} else {
			bookRepository.deleteBookById(book.getBookId());
			status.setStatusCode("true");
			status.setMessage("Delete successful");
		}
		DeleteBookResponse response = new DeleteBookResponse();
		response.setServiceStatus(status);
		return response;
	}
}
